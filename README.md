# Set up host file

This README will provide instructions on how to alter the hosts file on your machine.
After you complete this process, you will be able to access the given website in your browser.

## Mac
- Type ```terminal``` in spotlight or open it from applications > utilities > terminal
- Type ```$ sudo nano /private/etc/hosts```. You may need to enter your username and password.
- Type ```IP_ADDRESS DOMAIN_NAME```* on an empty line and hit ```Control-o``` to save the file.
- Press enter when the editor asks you to enter a file name and hit ```Control-x``` to close it.

## Windows
- Type ```win + r``` to open the application launcher.
- Insert ```notepad c:\Windows\System32\Drivers\etc\hosts```. This instructs notepad to launch the hosts file located in the specified path.
- Press ```ctrl + shift + enter``` to launch notepad as administrator.
- Type ```IP_ADDRESS DOMAIN_NAME```* on an empty line and hit ```ctrl + s``` to save the file.


## Linux
- Open a terminal window by pressing ```ctrl + alt + t```.
- Type ```$ sudo echo IP_ADDRESS DOMAIN_NAME >> /etc/hosts``` to insert a line to your hosts file.*  
If you want to make changes through a graphical user interface, you can open the file and perform edits the following way:
- Type ```$ sudo gedit /etc/hosts``` or ```$ sudo nano /etc/hosts``` to open your hosts file using your favorite editor.
- After you made the necessary changes, hit ```ctrl + s``` if you use gedit, or ```ctrl + x```, ```y``` and ```enter``` if you use nano.



\* Replace IP_ADDRESS and DOMAIN_NAME with the correct information. If you don't have this information, please contact the developer.

__NOTE__: Do not copy the ```$``` sign when entering commands in your terminal window.